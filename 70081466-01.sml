fun factorial(n: int ):int =
	if n = 0
	then 1
	else n * factorial(n-1);
(*working*)

fun pow (x, y) = 
	if y = 0
	then 1
	else x * pow(x , y-1);
(*working*)

fun gcd (a:int, b:int):int =
	if b=0
	then a
	else gcd (b, a mod b);
(*working*)
fun len (xs:int list):int =
		if null xs
		then 0
		else len(hd xs::tl xs)+1;
(*working*)
fun last(xs: int list): int option =
	if null xs
	then NONE
	else let val last = last(tl xs)
	in 
		if isSome last
		then SOME (1 + valOf last)
		else NONE
	end;
(*working*)
fun nth (xs: int list, n: int): int option =
	if null xs
	then NONE
	else if (hd xs = n)
	then SOME 1
	else let val tail = nth(tl xs, n)
	in
		if isSome tail
		then SOME (1+ valOf tail)
		else NONE
	end;
(*working*)

fun insert (xs: int list, x:int, n:int):int list = 
	if null xs 
	then [x]
	else
    if n = 0 then 
      x::tl xs
    else
      hd xs::(insert(tl xs,x,(n-1)));
(* working*)
(*i don't know if i need to add the element or replace it so i replace it*)

fun delete (xs: int list, x: int): int list = 
	case xs of
		[] => nil
		| hd:: tl=> if x = hd
							then delete (tl, x)
							else hd::delete(tl,x);
(*working*)
fun reverse (xs: int list): int list =
	let
		fun aux(xs, x) =
			case xs of
				[] => x
			  | (y::xs) => aux(xs, y :: x)
	in
		aux(xs, [])	
	end;

	(*working*)
	
fun palindrome (xs: int list): bool =
	let
		val xa = reverse(xs)
	in
		if xa = xs
		then true
		else false
	end;
(*working*)