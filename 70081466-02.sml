datatype natural = NEXT of natural | ZERO;
datatype tree = NODE of int * tree * tree | LEAF of int;

fun toInt (a: natural): int =
		case a of
			ZERO => 0
			| NEXT(a) => 1 + toInt(a);
(*working*)
val one = NEXT (ZERO);
val two = NEXT one;


fun add (a: natural, b: natural): natural =
	case a of
		ZERO => b
		| NEXT(a) => add(a,NEXT(b));
(*working *)

fun min (tree: tree): int option =
	case tree of
		LEAF(x) => SOME x
		| NODE(y, left, right) =>
		let
			val left = min (left)
			val right = min (right)
			fun min(a,b) = if a<b then a else b;
		in
			if valOf left < valOf right then left else right
	end ;

(*working*)
val tree = NODE(2,NODE(4,LEAF(3),LEAF(5)),LEAF(1));
(*tree de test*)

fun max (tree: tree): int option =
	case tree of
		LEAF(x) => SOME x
		| NODE(y, left, right) =>
		let
			val left = max (left)
			val right = max (right)
			fun max(a,b) = if a>b then a else b;
		in
			if valOf left > valOf right then left else right
	end ;
(*working*)

fun contains (tree: tree, x: int): bool =
  case tree of
    LEAF(y) => if y = x 
    then true 
    else false 
  | NODE(y, left, right) => if y = x then true else contains(left, x)
                                        orelse contains(right, x);

(*working*)

fun countLeaves (tree: tree): int =
	case tree of
		LEAF (x) => 1
		| NODE( _ ,left,right) => 1 + countLeaves left + countLeaves right;
(*working*)

fun countBranches (tree: tree): int =
	case tree of
		LEAF(x) => 0
		| NODE (x,left,right) => countLeaves(tree)-1;

(*working*)


fun height (tree: tree): int =
		case tree of
		LEAF(x) => 1
		| NODE (x,left,right) => 1 + Int.max (height left, height right);
(*working*)

fun toList (tree: tree): int list =
			case tree of
		LEAF(x) => [x]
		| NODE (x,left,right) => toList left @ toList right@ [x] ;
(*working*)
fun isBalanced (tree: tree): bool =
	case tree of
		LEAF x => true
		| NODE (x,left,right) => isBalanced left andalso isBalanced right andalso abs(height left - height right) <=1;
(*working*)

fun isBST (tree: tree): bool =
	case tree of
		LEAF x => true
		| NODE (x,left,right) =>
		if valOf(min(left)) > x
		then false
		else if valOf(max(right)) > x
		then false
		else if isBST(right) = true andalso isBST(left)= true
		then true
		else false;
(*not working*)
val tree2 = NODE(2,NODE(4,LEAF(5),LEAF(3)),LEAF(1));		